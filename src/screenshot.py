from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.proxy import Proxy, ProxyType
from PIL import Image

options = Options()
options.add_argument("--headless")
options.add_argument('--whitelisted-ips""')
options.add_argument("--window-size=1920,1080")

PROXY = "https://0.0.0.0:8080"
SSL = "https://0.0.0.0:8080"

proxy = Proxy({
    'proxyType': ProxyType.MANUAL,
    'httpProxy': PROXY,
    'ftpProxy': PROXY,
    'sslProxy': SSL,
    'noProxy': ''})


def get_screenshot(url, x_path=None):
    with webdriver.Firefox(options=options, proxy=proxy) as browser:
        browser.implicitly_wait(3)
        browser.get(url)
        title = browser.title
        filepath = "{}.png".format(title)
        total_width = browser.execute_script("return document.body.offsetWidth")
        total_height = browser.execute_script("return document.body.scrollHeight")
        browser.set_window_size(total_width, total_height)
        browser.save_screenshot(filepath)
        if x_path:
            page = browser.find_element_by_xpath(x_path)
            location = page.location
            size = page.size
            x = location['x']
            y = location['y']
            width = location['x'] + size['width']
            height = location['y'] + size['height']
            im = Image.open(filepath)
            im = im.crop((int(x), int(y), int(width), int(height)))
            im.save(filepath)
    print('Success ! See "{}.png" in the current folder.'.format(title))


if __name__ == "__main__":
    tests = {
        'url': 'http://www.lefigaro.fr/flash-actu/',
        'x_path': "//div[@class='fig-list-articles']"
    }
    get_screenshot(**tests)
